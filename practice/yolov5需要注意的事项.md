经验中需要注意的事项：
好了，现在我们已经略过了基础知识，让我们来看一下重要的内容：

- 别忘了规范化坐标
- 如果您的初始性能比预期的差得多，那么发生这种情况的最可能的原因（我在许多人身上也看到过这种情况）是您的预处理出了问题。这看起来很琐碎，但是有很多细节需要注意，特别是如果这是你的第一次。
- 有多种YoloV5型号（yolov5s、yolov5m、yolov5l、yolov5x），不要只选择最大的型号，因为它可能会过盈。从中等基线开始，并尝试改进它。
- 虽然我在512张图像上进行训练，但我发现将-img标志作为640传递可以提高性能
- 不要忘记加载预先训练的权重（-weights标志）。转移学习将极大地提高您的表现，并将为您节省大量的培训时间（在我的情况下，大约50个阶段，每个阶段大约需要20分钟！）
- Yolov5x需要大量内存，在批量大小为4的512个图像上进行训练时，它需要大约14GB的GPU内存（大多数GPU大约有8GB内存）。
- YoloV5已经使用了增强功能，您可以选择您喜欢和不喜欢的增强功能，您所需要做的就是处理YoloV5/data/hyp.scratch.yml
- 默认的yolov5训练脚本使用了权重和偏差([weights and biases](https://wandb.ai/site))，老实说，这给人留下了深刻的印象，它在模型训练时保存了所有的指标。但是，如果要关闭它，只需将`WANDB_MODE=“dryrun”` 添加到训练脚本标志中即可
- 我希望在前面找到的一件事是，YoloV5将大量有用的度量保存到目录YoloV5/runs/train/exp/。经过训练后，您可以找到“Conflusion_matrix.png”和“results.png”.