# YOLOv5 🚀 by Ultralytics, GPL-3.0 license
"""
Auto-anchor utils
"""
import logging
import os.path
import random

import numpy as np
import torch
import yaml
from scipy.cluster.vq import kmeans2
from tqdm import tqdm

from utils.general import LOGGER, colorstr, emojis

PREFIX = colorstr('AutoAnchor: ')


def check_anchor_order(m):
    # 根据YOLOv5 Detect（）模块m的步幅顺序检查锚定顺序，必要时进行纠正
    # Check anchor order against stride order for YOLOv5 Detect() module m, and correct if necessary
    a = m.anchors.prod(-1).view(-1)  # anchor area
    da = a[-1] - a[0]  # delta a
    ds = m.stride[-1] - m.stride[0]  # delta s
    if da.sign() != ds.sign():  # same order
        LOGGER.info(f'{PREFIX}Reversing anchor order')
        m.anchors[:] = m.anchors.flip(0)


def check_anchors(dataset, model, thr=4.0, imgsz=640):
    # Check anchor fit to data, recompute if necessary
    # 检查 anchor 是否与数据相符，必要时重新计算
    m = model.module.model[-1] if hasattr(model, 'module') else model.model[-1]  # Detect()
    shapes = imgsz * dataset.shapes / dataset.shapes.max(1, keepdims=True)
    scale = np.random.uniform(0.9, 1.1, size=(shapes.shape[0], 1))  # augment scale
    wh = torch.tensor(np.concatenate([l[:, 3:5] * s for s, l in zip(shapes * scale, dataset.labels)])).float()  # wh

    def metric(k):  # compute metric
        r = wh[:, None] / k[None]
        x = torch.min(r, 1 / r).min(2)[0]  # ratio metric
        best = x.max(1)[0]  # best_x
        aat = (x > 1 / thr).float().sum(1).mean()  # anchors above threshold
        bpr = (best > 1 / thr).float().mean()  # best possible recall
        return bpr, aat

    anchors = m.anchors.clone() * m.stride.to(m.anchors.device).view(-1, 1, 1)  # current anchors
    bpr, aat = metric(anchors.cpu().view(-1, 2))
    logging.error("________________bpr: %s" % bpr)
    s = f'\n{PREFIX}{aat:.2f} anchors/target, {bpr:.3f} Best Possible Recall (BPR). '
    if bpr > 0.98 and 0:  # threshold to recompute  # yolov5的网络结构配置文件(如:models/yolov5s.yaml)已经给出了默认的一组anchor，只有当bpr小于0.98时才会重新计算anchor
        LOGGER.info(emojis(f'{s}Current anchors are a good fit to dataset ✅'))
    else:
        LOGGER.info(emojis(f'{s}Anchors are a poor fit to dataset ⚠️, attempting to improve...'))
        na = m.anchors.numel() // 2  # number of anchors
        try:
            anchors = kmean_anchors(dataset, n=na, img_size=imgsz, thr=thr, gen=1000, verbose=False)
        except Exception as e:
            LOGGER.info(f'{PREFIX}ERROR: {e}')
        new_bpr = metric(anchors)[0]
        if new_bpr > bpr:  # replace anchors
            anchors = torch.tensor(anchors, device=m.anchors.device).type_as(m.anchors)
            m.anchors[:] = anchors.clone().view_as(m.anchors) / m.stride.to(m.anchors.device).view(-1, 1, 1)  # loss
            check_anchor_order(m)
            LOGGER.info(f'{PREFIX}New anchors saved to model. Update model *.yaml to use these anchors in the future.')
        else:
            LOGGER.info(f'{PREFIX}Original anchors better than new anchors. Proceeding with original anchors.')


def kmean_anchors(dataset='./data/coco128.yaml', n=9, img_size=640, thr=4.0, gen=1000, verbose=True):
    """ Creates kmeans-evolved anchors from training dataset

        Arguments:
            dataset: path to data.yaml, or a loaded dataset
            n: number of anchors
            img_size: image size used for training
            thr: anchor-label wh ratio threshold hyperparameter hyp['anchor_t'] used for training, default=4.0
            gen: generations to evolve anchors using genetic algorithm
            verbose: print all results

        Return:
            k: kmeans evolved anchors

        Usage:
            from utils.autoanchor import *; _ = kmean_anchors()
    """
    from scipy.cluster.vq import kmeans

    thr = 1 / thr

    def metric(k, wh):  # compute metrics  # metric根据默认anchor和wh来具体计算bpr,aat(anchors above threshold)两个指标。
        r = wh[:, None] / k[None]  # 计算每个gt框的宽高和所有这9个anchor的宽高的比例值，得到的r其shape为(N,9,2)。
        x = torch.min(r, 1 / r).min(2)[0]  # ratio metric  # w_gt/w_anchor或者h_gt/h_anchor这个比例值可能大于1也可能小于1，通过torch.min(r,1./r)的方式统一到<=1的情形，然后再从中选取较小的这个值。
        # x = wh_iou(wh, torch.tensor(k))  # iou metric
        return x, x.max(1)[0]  # x, best_x: 每个gt框选择匹配宽高比例值最好的那一个值

    # 计算聚类和遗传算法出来的anchor和真实bbox之间的重合程度
    def anchor_fitness(k):  # mutation fitness
        _, best = metric(torch.tensor(k, dtype=torch.float32), wh)
        return (best * (best > thr).float()).mean()  # fitness

    def print_results(k, verbose=True):  # 计算每一行的乘积(w*h),然后排序得到排序后的k
        k = k[np.argsort(k.prod(1))]  # sort small to large
        x, best = metric(k, wh0)
        bpr, aat = (best > thr).float().mean(), (x > thr).float().mean() * n  # best possible recall, anch > thr
        s = f'{PREFIX}thr={thr:.2f}: {bpr:.4f} best possible recall, {aat:.2f} anchors past thr\n' \
            f'{PREFIX}n={n}, img_size={img_size}, metric_all={x.mean():.3f}/{best.mean():.3f}-mean/best, ' \
            f'past_thr={x[x > thr].mean():.3f}-mean: '
        for i, x in enumerate(k):
            s += '%i,%i, ' % (round(x[0]), round(x[1]))
        if verbose:
            LOGGER.info(s[:-2])
        return k

    if isinstance(dataset, str):  # *.yaml file
        with open(dataset, errors='ignore') as f:
            data_dict = yaml.safe_load(f)  # model dict
        from utils.datasets import LoadImagesAndLabels
        # dataset = LoadImagesAndLabels(data_dict['train'], augment=True, rect=True)
        dataset = LoadImagesAndLabels(os.path.join(data_dict['path'], data_dict['train']), augment=True, rect=True)

    # Get label wh
    shapes = img_size * dataset.shapes / dataset.shapes.max(1, keepdims=True)
    wh0 = np.concatenate([l[:, 3:5] * s for s, l in zip(shapes, dataset.labels)])  # wh  # shapes: 各张图片的大小  labels: 矩形框

    # Filter
    i = (wh0 < 3.0).any(1).sum()
    i6 = (wh0 < 6.0).any(1).sum()
    i9 = (wh0 < 12.0).any(1).sum()
    if i:
        LOGGER.info(f'{PREFIX}WARNING: Extremely small objects found. {i} of {len(wh0)} labels are < 3 pixels in size.')
        LOGGER.info(f'{PREFIX}警告：发现非常小的物体。  {len(wh0)} 个 标签中 {i} 个 小于 3 像素值.')
        LOGGER.info(f'{PREFIX}警告：发现非常小的物体。  {len(wh0)} 个 标签中 {i6} 个 小于 6 像素值.')
        LOGGER.info(f'{PREFIX}警告：发现非常小的物体。  {len(wh0)} 个 标签中 {i9} 个 小于 12 像素值.')


    wh = wh0[(wh0 >= 2.0).any(1)]  # filter > 2 pixels  # 存储训练数据中所有gt框的宽高, 并且其像素值大于2
    # wh = wh * (np.random.rand(wh.shape[0], 1) * 0.9 + 0.1)  # multiply by random scale 0-1

    # Kmeans calculation
    LOGGER.info(f'{PREFIX}Running kmeans for {n} anchors on {len(wh)} points...')
    LOGGER.info(f'{PREFIX} 运行 kmeans 算法用于 {n} anchors 从 {len(wh)} 点中...')
    s = wh.std(0)  # sigmas for whitening  # 标准化一组观察值，为了使每个特征具有单位方差, 那就奇怪了, 为什么不直接调用 whiten 呢?
    # def kmeans(obs, k_or_guess, iter=20, thresh=1e-5, check_finite=True, *, seed=None)
    # k-means聚类的另一种实现方式，使用更多的方法生成初始质心，但不使用失真变化阈值作为停止标准。
    # def kmeans2(data, k, iter=10, thresh=1e-5, minit='random', missing='warn', check_finite=True, *, seed=None)
    # k, dist = kmeans(wh / s, n, iter=30)  # points, mean distance

    k, dist = kmeans2(wh / s, n, iter=100, thresh=1e-5, minit='++', missing='warn')  # points, mean distance
    print("k: %s, dist: %s" % (k, dist))
    assert len(k) == n, f'{PREFIX}ERROR: scipy.cluster.vq.kmeans requested {n} points but returned only {len(k)}'
    k *= s
    print("k: %s, dist: %s" % (k, dist))
    wh = torch.tensor(wh, dtype=torch.float32)  # filtered
    wh0 = torch.tensor(wh0, dtype=torch.float32)  # unfiltered
    k = print_results(k, verbose=False)
    print_results(k, verbose=True)
    print("--------------  k-means  ------------")

    # Plot
    # k, d = [None] * 20, [None] * 20
    # for i in tqdm(range(1, 21)):
    #     k[i-1], d[i-1] = kmeans(wh / s, i)  # points, mean distance
    # fig, ax = plt.subplots(1, 2, figsize=(14, 7), tight_layout=True)
    # ax = ax.ravel()
    # ax[0].plot(np.arange(1, 21), np.array(d) ** 2, marker='.')
    # fig, ax = plt.subplots(1, 2, figsize=(14, 7))  # plot wh
    # ax[0].hist(wh[wh[:, 0]<100, 0],400)
    # ax[1].hist(wh[wh[:, 1]<100, 1],400)
    # fig.savefig('wh.png', dpi=200)

    # Evolve
    # YOLOV5改进遗传算法
    npr = np.random
    f, sh, mp, s = anchor_fitness(k), k.shape, 0.9, 0.1  # fitness, generations, mutation prob, sigma
    pbar = tqdm(range(gen), desc=f'{PREFIX}Evolving anchors with Genetic Algorithm:')  # progress bar
    for _ in pbar:
        v = np.ones(sh)
        while (v == 1).all():  # mutate until a change occurs (prevent duplicates)
            v = ((npr.random(sh) < mp) * random.random() * npr.randn(*sh) * s + 1).clip(0.3, 3.0)
        kg = (k.copy() * v).clip(min=2.0)
        fg = anchor_fitness(kg)
        if fg > f:
            f, k = fg, kg.copy()
            pbar.desc = f'{PREFIX}Evolving anchors with Genetic Algorithm: fitness = {f:.4f}'
            if verbose:
                print_results(k, verbose)

    return print_results(k)
