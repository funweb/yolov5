import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import imageio


def plot_img(power, y_max):
    """
    每次画一幅图片, 最后将图片组合为数组, 从而绘制为 gif 动图
    @param power: TODO
    @param y_max: TODO
    @return: image array
    """
    # Data for plotting
    t = np.arange(0.0, 100, 1)
    s = t ** power

    fig, ax = plt.subplots(figsize=(10, 5))
    ax.plot(t, s)
    ax.grid()
    ax.set(xlabel='X', ylabel='x^{}'.format(power),
           title='Powers of x')

    # IMPORTANT ANIMATION CODE HERE
    # Used to keep the limits constant
    ax.set_ylim(0, y_max)

    # Used to return the plot as an image rray
    fig.canvas.draw()  # draw the canvas, cache the renderer
    image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
    image = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))

    return image


if __name__ == '__main__':
    kwargs_write = {'fps': 1.0, 'quantizer': 'nq'}
    imgs_array = [plot_img(i / 4, 100) for i in range(3)]
    imageio.mimsave('./powers.gif', imgs_array, fps=kwargs_write["fps"])
