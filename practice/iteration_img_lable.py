import os
from tqdm import tqdm


# 为了 yolov4 的数据集做准备, 便于生成数据和标签
def iter_data(base_dir, data_dir_name):
    data_dir = os.path.join(base_dir, data_dir_name)
    files_list = os.listdir(data_dir)
    sFile_list_img = []
    sFile_list_label = []
    for file_name in files_list:
        if file_name.endswith(".jpg"):
            sFile_list_img.append(os.path.join(data_dir, file_name + "\n"))
        if file_name.endswith(".txt"):
            sFile_list_label.append(os.path.join(data_dir, file_name+"\n"))
    sFile_list_img[-1] = sFile_list_img[-1].strip('\n')
    sFile_list_label[-1] = sFile_list_label[-1].strip('\n')
    return sFile_list_img, sFile_list_label


if __name__ == '__main__':
    base_dir = "F:/datasets/trim500_train_test"  # 总的目录, 也就是生成训练和测试集文件的目录
    data_dir_names = ["train", "test"]
    for data_dir_name in tqdm(data_dir_names):
        sFile_list_img, sFile_list_label = iter_data(base_dir, data_dir_name)

        with open(os.path.join(base_dir, "%s_img_list.txt" % (data_dir_name)), "w", encoding="utf-8") as fw:
            for file in sFile_list_img:
                fw.write(file)
        with open(os.path.join(base_dir, "%s_label_list.txt" % (data_dir_name)), "w", encoding="utf-8") as fw:
            for file in sFile_list_img:
                fw.write(file)

