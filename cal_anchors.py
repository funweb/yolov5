from utils.autoanchor import kmean_anchors, check_anchors
from models.yolo import Model
from utils.general import check_img_size
import yaml
from utils.datasets import create_dataloader
from utils.general import (LOGGER, check_dataset, check_file, check_git_status, check_img_size, check_requirements,
                           check_suffix, check_yaml, colorstr, get_latest_run, increment_path, init_seeds,
                           intersect_dicts, labels_to_class_weights, labels_to_image_weights, methods, one_cycle,
                           print_args, print_mutation, strip_optimizer)

"""
256: 4,3, 6,6, 9,10, 16,12, 16,20, 31,17, 26,35, 63,26, 44,59
320: 5,4, 7,8, 11,12, 20,15, 22,28, 47,24, 35,46, 94,41, 54,78
640: 11,8, 15,15, 23,23, 39,29, 42,53, 77,44, 70,87, 236,71, 119,162
"""


def kmean_anchors_LY(dataset='./data/coco128.yaml', n=9, img_size=640, thr=4.0, gen=1000, verbose=True):
    ans = kmean_anchors(dataset, n, img_size, thr, gen, verbose)
    return ans


def check_anchors_LY(thr=4.0, imgsz=640):

    cfg = "configure/cfgs/yolov5s_anchor.yaml"
    hyp = "configure/hyps/hyp.scratch.yaml"
    if isinstance(hyp, str):
        with open(hyp, errors='ignore') as f:
            hyp = yaml.safe_load(f)  # load hyps dict

    model = Model(cfg, ch=3, nc=13, anchors=hyp.get('anchors'))  # create
    # Image size
    gs = max(int(model.stride.max()), 32)  # grid size (max stride)
    imgsz = check_img_size(img_size, gs, floor=gs * 2)  # verify imgsz is gs-multiple

    # Trainloader
    data = "configure/trim500_train_test.yaml"
    data_dict = check_dataset(data)  # check if None
    train_path, val_path = data_dict['train'], data_dict['val']
    batch_size = 2
    WORLD_SIZE = 1
    single_cls = False
    workers = 8
    train_loader, dataset = create_dataloader(train_path, imgsz, batch_size // WORLD_SIZE, gs, single_cls,
                                              hyp=hyp, augment=True, cache=None, rect=False, rank=-1,
                                              workers=workers, image_weights=False, quad=False,
                                              prefix=colorstr('train: '), shuffle=True)
    check_anchors(dataset, model, thr, imgsz)


if __name__ == '__main__':
    dataset = "./configure/trim500_train_test.yaml"
    n = 9
    img_size = 1280
    thr = 4.0
    gen = 1000
    verbose = True
    ans = kmean_anchors(dataset, n, img_size, thr, gen, verbose)
    print(ans)

    # check_anchors_LY(thr, img_size)


