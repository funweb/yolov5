import os.path
import sys
import numpy as np
import posixpath

import argparse
import os
import platform
import shutil
import time
import json
from pathlib import Path
import cv2
import torch
from utils.general import (LOGGER, box_iou, check_dataset, check_img_size, check_requirements, check_yaml,
                           coco80_to_coco91_class, colorstr, increment_path, non_max_suppression, print_args,
                           scale_coords, xywh2xyxy, xyxy2xywh, xywhn2xyxy)

from utils.metrics import ConfusionMatrix, ap_per_class


def xywh_to_xyxy_for_MOT(xywh):
    """
    因为 MOT 是 左上宽高, 因此这样计算.
    @param xywh:
    @return:
    """
    x1 = xywh[:, 0]
    y1 = xywh[:, 1]
    x2 = xywh[:, 0] + xywh[:, 2]
    y2 = xywh[:, 1] + xywh[:, 3]

    return np.array([x1, y1, x2, y2]).T


def compute_color_for_labels(label):
    """
    Simple function that adds fixed color depending on the class
    """
    palette = (2 ** 11 - 1, 2 ** 15 - 1, 2 ** 20 - 1)
    color = [int((p * (label ** 2 - label + 1)) % 255) for p in palette]
    return tuple(color)


def draw_boxes(img, bbox, identities=None, categories=None, names=None, offset=(0, 0)):
    for i, box in enumerate(bbox):
        x1, y1, x2, y2 = [int(i) for i in box]
        x1 += offset[0]
        x2 += offset[0]
        y1 += offset[1]
        y2 += offset[1]
        # box text and bar
        cat = int(categories[i]) if categories is not None else 0

        id = int(identities[i]) if identities is not None else 0

        color = compute_color_for_labels(id)

        label = f'{names[cat]}'
        t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_PLAIN, 2, 2)[0]
        cv2.rectangle(img, (x1, y1), (x2, y2), color, 3)
        cv2.rectangle(
            img, (x1, y1), (x1 + t_size[0] + 3, y1 + t_size[1] + 4), color, -1)
        cv2.putText(img, label, (x1, y1 +
                                 t_size[1] + 4), cv2.FONT_HERSHEY_PLAIN, 1.5, [255, 255, 255], 1)
    return img



device = "cpu"


def get_gt_from_MOT16(gt_dir, file_name="gt.txt"):
    """
    因为要转化为符合 yolo 排列
    @param det_dir:
    @param file_name:
    @return:
    """
    gt_path = os.path.join(gt_dir, file_name)
    all_gt = np.loadtxt(gt_path, delimiter=',')
    all_gt = all_gt[all_gt[:, 8] == 1]  # 暂时只要为1的
    gt_xyxy = xywh_to_xyxy_for_MOT(all_gt[:, 2:6])
    # [img_index, class, x, y, w, h]

    gt_c_xyxy = np.hstack((all_gt[:, 7:8], gt_xyxy))
    gt_i_c_xyxy = np.hstack((all_gt[:, 0:1], gt_c_xyxy))

    return gt_i_c_xyxy


def get_det_from_MOT16(det_dir, file_name="gt.txt"):
    """
    因为要转化为符合 yolo 排列
    @param det_dir:
    @param file_name:
    @return:
    """
    det_path = os.path.join(det_dir, file_name)
    all_det = np.loadtxt(det_path, delimiter=',')
    all_det = all_det[all_det[:, 6] <= 1]  # 暂时只要置信度在 0--1 之间的
    all_det = all_det[all_det[:, 6] > 0]  # 暂时只要置信度在 0--1 之间的
    det_xyxy = xywh_to_xyxy_for_MOT(all_det[:, 2:6])
    det_i_xyxy = np.hstack((all_det[:, 0:1], det_xyxy))
    det_i_xyxy_c = np.hstack((det_i_xyxy, all_det[:, 6:7]))
    det_i_xyxy_c_c = np.hstack((det_i_xyxy_c, np.ones((len(all_det), 1))))

    return det_i_xyxy_c_c


def process_batch(detections, labels, iouv):
    """6.9、计算混淆矩阵、计算correct、生成stats
        初始化预测评定 niou为iou阈值的个数  Assign all predictions as incorrect
        correct = [pred_obj_num, 10] = [300, 10]  全是False

    Return correct predictions matrix. Both sets of boxes are in (x1, y1, x2, y2) format.
    Arguments:
        detections (Array[N, 6]), x1, y1, x2, y2, conf, class
        labels (Array[M, 5]), class, x1, y1, x2, y2
    Returns:
        correct (Array[N, 10]), for 10 IoU levels
    """
    correct = torch.zeros(detections.shape[0], iouv.shape[0], dtype=torch.bool, device=iouv.device)
    iou = box_iou(labels[:, 1:], detections[:, :4])
    x = torch.where((iou >= iouv[0]) & (labels[:, 0:1] == detections[:, 5]))  # IoU above threshold and classes match
    if x[0].shape[0]:
        matches = torch.cat((torch.stack(x, 1), iou[x[0], x[1]][:, None]), 1).cpu().numpy()  # [label, detection, iou]
        if x[0].shape[0] > 1:
            matches = matches[matches[:, 2].argsort()[::-1]]
            matches = matches[np.unique(matches[:, 1], return_index=True)[1]]
            # matches = matches[matches[:, 2].argsort()[::-1]]
            matches = matches[np.unique(matches[:, 0], return_index=True)[1]]
        matches = torch.Tensor(matches).to(iouv.device)
        correct[matches[:, 1].long()] = matches[:, 2:3] >= iouv
    return correct



def save_one_txt(predn, save_conf, shape, file):
    # Save one txt result
    # gn = [w, h, w, h] 对应图片的宽高  用于后面归一化
    gn = torch.tensor(shape)[[1, 0, 1, 0]]  # normalization gain whwh
    for *xyxy, conf, cls in predn.tolist():
        # xyxy -> xywh 并作归一化处理
        xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
        line = (cls, *xywh, conf) if save_conf else (cls, *xywh)  # label format
        # 保存预测类别和坐标值到对应图片image_name.txt文件中
        with open(file, 'a') as f:
            f.write(('%g ' * len(line)).rstrip() % line + '\n')


def save_one_json(predn, jdict, path, class_map):
    """
    6.8、将预测信息保存到coco格式的json字典(后面存入json文件) Append to pycocotools JSON dictionary
    类似: [{"image_id": 42, "category_id": 18, "bbox": [258.15, 41.29, 348.26, 243.78], "score": 0.236}, ...
    @param predn:
    @param jdict:
    @param path:
    @param class_map:
    @return:
    """
    # Save one JSON result {"image_id": 42, "category_id": 18, "bbox": [258.15, 41.29, 348.26, 243.78], "score": 0.236}
    image_id = int(path.stem) if path.stem.isnumeric() else path.stem  # 获取图片id
    box = xyxy2xywh(predn[:, :4])  # xywh  # 获取预测框 并将xyxy转为xywh格式
    # 之前的的xyxy格式是左上角右下角坐标  xywh是中心的坐标和宽高
    # 而coco的json格式的框坐标是xywh(左上角坐标 + 宽高)
    # 所以这行代码是将中心点坐标 -> 左上角坐标  xy center to top-left corner
    box[:, :2] -= box[:, 2:] / 2  # xy center to top-left corner
    # image_id: 图片id 即属于哪张图片
    # category_id: 类别 coco91class()从索引0~79映射到索引0~90
    # bbox: 预测框坐标
    # score: 预测得分
    for p, b in zip(predn.tolist(), box.tolist()):
        jdict.append({'image_id': image_id,
                      'category_id': class_map[int(p[5])],
                      'bbox': [round(x, 3) for x in b],
                      'score': round(p[4], 5)})


def get_all_img_name(img_dir):
    """
    获得所有测试文件
    @param img_dir: 待测试图片文件夹
    @return: 图片文件字典, 相当于编码后的
    """
    frame_idx = 0
    all_img_dict = {}

    for file in os.listdir(img_dir):  # 逐张图片遍历
        (filename, extension) = os.path.splitext(file)  # 文件名 和 后缀名
        if extension == ".jpg":
            frame_idx = frame_idx + 1
            all_img_dict.update({
                filename: frame_idx
            })

    return all_img_dict


def get_gt_from_YOLO(gt_dir, img_dict):
    """
    从图片文件夹遍历图片获得响应的标签
    1. 如果不存在对应的标签文件, 暂不考虑
    2. 因为要转换坐标表示, 因此图片大小一致
    @param gt_dir: 真实标签所在路径
    @param img_dict: 图片编码字典
    @return: 符合格式的 tensor
    """

    frame_idx_list = []

    for filename in img_dict:
        with open(os.path.join(gt_dir, filename+".txt"), "r") as fr:  # 图片逐个标签遍历
            lines = fr.readlines()

            for line in lines:
                # remove linebreak which is the last character of the string
                currentLine = line[:-1]
                data = currentLine.split(" ")
                data.insert(0, img_dict[filename])  # 添加当前帧的索引
                # add item to the list
                frame_idx_list.append(data)

    frame_idx_list = np.array(frame_idx_list, dtype="float64")

    gt_i_c_xyxy = torch.tensor(frame_idx_list, dtype=torch.float64)
    bbox = xywhn2xyxy(gt_i_c_xyxy[:, 2:6], w=203, h=248)
    gt_i_c_xyxy[:, 2:6] = bbox

    return gt_i_c_xyxy


def get_det_from_YOLO(det_dir, img_dict):
    """
    从图片文件夹遍历图片获得响应的标签
    1. 如果不存在对应的标签文件, 暂不考虑
    @param det_dir: 真实标签所在路径
    @param img_dict: 图片编码字典
    @return: 符合格式的 tensor
    """

    frame_idx_list = []

    for filename in img_dict:
        with open(os.path.join(det_dir, filename + ".txt"), "r") as fr:  # 图片逐个标签遍历
            lines = fr.readlines()

            for line in lines:
                # remove linebreak which is the last character of the string
                currentLine = line[:-1]
                data = currentLine.split(" ")
                data.insert(0, img_dict[filename])  # 添加当前帧的索引
                # add item to the list
                frame_idx_list.append(data)

    frame_idx_list = np.array(frame_idx_list, dtype="float64")

    det_i_xyxy_c_c = torch.tensor(frame_idx_list, dtype=torch.float64)
    bbox = xywhn2xyxy(det_i_xyxy_c_c[:, 1:5], w=203, h=248)
    det_i_xyxy_c_c[:, 1:5] = bbox

    return det_i_xyxy_c_c


def get_gt_det(gt_dir, det_dir, all_img_name_dict):
    """
    载入不同类型的数据格式, 返回符合验证格式的数据
    @param gt_dir: 真实标签所在文件夹
    @param det_dir: 检测标签所在文件夹
    @return: 符合数据的格式类型
    """
    # 因为这种数据格式就是按照 frame ID 编码而成, 因此现无需用到 all_img_name_dict
    # gt_i_c_xyxy = get_gt_from_MOT16(gt_dir)
    # det_i_xyxy_c_c = get_det_from_MOT16(det_dir)

    gt_i_c_xyxy = get_gt_from_YOLO(gt_dir, img_dict=all_img_name_dict)
    det_i_xyxy_c_c = get_det_from_YOLO(det_dir, img_dict=all_img_name_dict)

    return gt_i_c_xyxy, det_i_xyxy_c_c


if __name__ == '__main__':
    # 首选要知道一共有多少帧, 然后依次遍历返回 预测标签, 真实标签, 图片路径等;
    # 最简单的方法就是遍历图片, 然后将帧序号转为索引值.
    # 1. 遍历图片,
    base_dir = os.path.join("F:/datasets/test/plot_img")

    img_dir = os.path.join(base_dir, "img")
    gt_dir = os.path.join(base_dir, "gt")
    det_dir = os.path.join(base_dir, "det")

    result_dir = os.path.join(base_dir, "result")

    # 获得图片 dict 文件
    all_img_name_dict = get_all_img_name(img_dir)
    # 分别获得 gt 和 det 信息;
    gt_i_c_xyxy, det_i_xyxy_c_c = get_gt_det(gt_dir, det_dir, all_img_name_dict)


    names = {0: 'person', 1: 'bicycle', 2: 'car', 3: 'motorcycle', 4: 'airplane', 5: 'bus', 6: 'train', 7: 'truck',
             8: 'boat', 9: 'traffic light', 10: 'fire hydrant', 11: 'stop sign', 12: 'parking meter', 13: 'bench',
             14: 'bird', 15: 'cat', 16: 'dog', 17: 'horse', 18: 'sheep', 19: 'cow', 20: 'elephant', 21: 'bear',
             22: 'zebra', 23: 'giraffe', 24: 'backpack', 25: 'umbrella', 26: 'handbag', 27: 'tie', 28: 'suitcase',
             29: 'frisbee', 30: 'skis', 31: 'snowboard', 32: 'sports ball', 33: 'kite', 34: 'baseball bat',
             35: 'baseball glove', 36: 'skateboard', 37: 'surfboard', 38: 'tennis racket', 39: 'bottle',
             40: 'wine glass', 41: 'cup', 42: 'fork', 43: 'knife', 44: 'spoon', 45: 'bowl', 46: 'banana', 47: 'apple',
             48: 'sandwich', 49: 'orange', 50: 'broccoli', 51: 'carrot', 52: 'hot dog', 53: 'pizza', 54: 'donut',
             55: 'cake', 56: 'chair', 57: 'couch', 58: 'potted plant', 59: 'bed', 60: 'dining table', 61: 'toilet',
             62: 'tv', 63: 'laptop', 64: 'mouse', 65: 'remote', 66: 'keyboard', 67: 'cell phone', 68: 'microwave',
             69: 'oven', 70: 'toaster', 71: 'sink', 72: 'refrigerator', 73: 'book', 74: 'clock', 75: 'vase',
             76: 'scissors', 77: 'teddy bear', 78: 'hair drier', 79: 'toothbrush'}
    seen = 0  # 统计图片数量
    nc = 80
    # 初始化混淆矩阵
    confusion_matrix = ConfusionMatrix(nc=nc)
    jdict, stats, ap, ap_class = [], [], [], []
    iouv = torch.linspace(0.5, 0.95, 10).to(device)  # iou vector for mAP@0.5:0.95
    niou = iouv.numel()  # mAP@0.5:0.95 iou个数=10个

    # 这里的思路是根据图片进行遍历. 当然, 也可以有其他的方式, 这是取巧
    for img_name in all_img_name_dict:  # for every frame
        # print(img_name)
        current_img_index = int(all_img_name_dict[img_name])

        img_origin = cv2.imread(os.path.join(base_dir, img_dir, img_name+".jpg"), 1)
        # img = torch.from_numpy(img).to(device)
        img_gt = img_origin.copy()
        img_det = img_origin.copy()

        current_gt = gt_i_c_xyxy[gt_i_c_xyxy[:, 0] == current_img_index]
        current_gt = current_gt[:, 1:]
        # current_gt[:, 1] = 1
        current_det = det_i_xyxy_c_c[det_i_xyxy_c_c[:, 0] == current_img_index]
        current_det = current_det[:, 1:]
        # current_det[:, 1] = 1

        draw_boxes(img_gt, current_gt[:, 1:5], names=names)
        draw_boxes(img_det, current_det[:, 0:4], names=names)

        cv2.imwrite(os.path.join(base_dir, "output", "gt", img_name+".jpg"), img_gt)
        cv2.imwrite(os.path.join(base_dir, "output", "det", img_name+".jpg"), img_det)

        nl = len(current_gt)  # 当前图片的gt个数
        tcls = current_gt[:, 0].tolist() if nl else []  # target class  # gt_类别, list

        # 是否需要获取形状并改变呢?
        # TODO: 此次实验同意大小, 无需更改

        seen += 1  # 统计测试图片数量 +1

        # 如果预测为空，则添加空的信息到stats里
        if len(current_det) == 0:
            if nl:
                stats.append((torch.zeros(0, niou, dtype=torch.bool), torch.Tensor(), torch.Tensor(), tcls))
            continue

        # Evaluate
        if nl:
            correct = process_batch(current_det, current_gt, iouv)  # 正确的
            confusion_matrix.process_batch(current_det, current_gt)
        else:
            correct = torch.zeros(current_det.shape[0], niou, dtype=torch.bool)

        # 将每张图片的预测结果统计到stats中
        # Append statistics(correct, conf, pcls, tcls)   bs个(correct, conf, pcls, tcls)
        # correct: [pred_num, 10] bool 当前图片每一个预测框在每一个iou条件下是否是TP
        # pred[:, 4]: [pred_num, 1] 当前图片每一个预测框的conf
        # pred[:, 5]: [pred_num, 1] 当前图片每一个预测框的类别
        # tcls: [gt_num, 1] 当前图片所有gt框的class
        stats.append((correct.cpu(), current_det[:, 4].cpu(), current_det[:, 5].cpu(), tcls))  # (correct, conf, pcls, tcls)

        # 6.6、保存预测信息到txt文件  runs\test\exp7\labels\image_name.txt
        # Save/log
        save_one_txt(current_det, save_conf=True, shape=img_origin.shape, file=os.path.join(result_dir, "onetxt", "%s.txt" % str(all_img_name_dict[img_name])))
        # save_one_json(current_det, jdict, path=os.path.join(result_dir, "onejson", img_name), class_map=list(range(1000)))  # append to COCO-JSON dictionary
        # callbacks.run('on_val_image_end', pred, predn, path, names, im[si])  # 这一句话以来太多, 不知道有啥用, 暂时不要.

    # 6.11、计算mAP
    # 统计stats中所有图片的统计结果 将stats列表的信息拼接到一起
    # stats(concat后): list{4} correct, conf, pcls, tcls  统计出的整个数据集的GT
    # correct [img_sum, 10] 整个数据集所有图片中所有预测框在每一个iou条件下是否是TP  [1905, 10]
    # conf [img_sum] 整个数据集所有图片中所有预测框的conf  [1905]
    # pcls [img_sum] 整个数据集所有图片中所有预测框的类别   [1905]
    # tcls [gt_sum] 整个数据集所有图片所有gt框的class     [929]
    # Compute metrics
    stats_bak = stats.copy()
    stats = [np.concatenate(x, 0) for x in zip(*stats)]  # to numpy
    # stats[0].any(): stats[0]是否全部为False, 是则返回 False, 如果有一个为 True, 则返回 True
    if len(stats) and stats[0].any():
        # 根据上面的统计预测结果计算p, r, ap, f1, ap_class（ap_per_class函数是计算每个类的mAP等指标的）等指标
        # p: [nc] 最大平均f1时每个类别的precision
        # r: [nc] 最大平均f1时每个类别的recall
        # ap: [71, 10] 数据集每个类别在10个iou阈值下的mAP
        # f1 [nc] 最大平均f1时每个类别的f1
        # ap_class: [nc] 返回数据集中所有的类别index
        tp, fp, p, r, f1, ap, ap_class = ap_per_class(*stats, plot=True, save_dir=os.path.join(result_dir), names=names)
        # ap50: [nc] 所有类别的mAP@0.5   ap: [nc] 所有类别的mAP@0.5:0.95
        ap50, ap = ap[:, 0], ap.mean(1)  # AP@0.5, AP@0.5:0.95
        # mp: [1] 所有类别的平均precision(最大f1时)
        # mr: [1] 所有类别的平均recall(最大f1时)
        # map50: [1] 所有类别的平均mAP@0.5
        # map: [1] 所有类别的平均mAP@0.5:0.95
        mp, mr, map50, map = p.mean(), r.mean(), ap50.mean(), ap.mean()
        # nt: [nc] 统计出整个数据集的gt框中数据集各个类别的个数
        nt = np.bincount(stats[3].astype(np.int64), minlength=nc)  # number of targets per class
    else:
        nt = torch.zeros(1)

    # 6.12、print打印各项指标
    # Print results  数据集图片数量 + 数据集gt框的数量 + 所有类别的平均precision +
    #                所有类别的平均recall + 所有类别的平均mAP@0.5 + 所有类别的平均mAP@0.5:0.95
    # Print results
    pf = '%20s' + '%11i' * 2 + '%11.3g' * 4  # print format
    # LOGGER.info(pf % ('all', seen, nt.sum(), mp, mr, map50, map))
    print(pf % ('all', seen, nt.sum(), mp, mr, map50, map))

    # Print results per class
    # 细节展示每个类别的各个指标  类别 + 数据集图片数量 + 这个类别的gt框数量 + 这个类别的precision +
    #                        这个类别的recall + 这个类别的mAP@0.5 + 这个类别的mAP@0.5:0.95
    if (True or (nc < 50)) and nc > 1 and len(stats):
        for i, c in enumerate(ap_class):
            LOGGER.info(pf % (names[c], seen, nt[c], p[i], r[i], ap50[i], ap[i]))


        confusion_matrix.plot(save_dir=os.path.join(result_dir), names=names)
