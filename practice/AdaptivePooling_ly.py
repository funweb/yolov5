from torch import nn
import torch

# + LY_customer
# EPSANet
def conv(in_planes, out_planes, kernel_size=3, stride=1, padding=1, dilation=1, groups=1):
    """standard convolution with padding"""
    """
    Conv2d 参数解析:
    in_channels: int, 输入的通道数目
    out_channels: int, 输出的通道数目
    kernel_size: _size_2_t, 卷积核的大小, 类型为int或者元组. 当卷积是方形的时候, 只需要一个整数即可, 否则, 输入元组表示 高和宽.
    stride: _size_2_t = 1,  步长, 
    padding: Union[str, _size_2_t] = 0, 填充多少层0, 3x3图片padding=1的时候, 操作后变为5x5
    dilation: _size_2_t = 1,  卷积核之间的距离
    groups: int = 1,  控制输入和输出之间的连接
    bias: bool = True,  是否将一个 学习到的 bias 增加输出中，默认是 True 。
    padding_mode: str = 'zeros',  # TODO: refine this type  字符串类型，接收的字符串只有 “zeros” 和 “circular”
    device=None,
    dtype=None
    """
    return nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride,
                     padding=padding, dilation=dilation, groups=groups, bias=False)


class SEWeightModule(nn.Module):

    def __init__(self, channels, reduction=16):
        super(SEWeightModule, self).__init__()
        self.avg_pool = nn.AdaptiveAvgPool2d(1)
        self.fc1 = nn.Conv2d(channels, channels // reduction, kernel_size=1, padding=0)
        self.relu = nn.ReLU(inplace=True)
        self.fc2 = nn.Conv2d(channels // reduction, channels, kernel_size=1, padding=0)
        self.sigmoid = nn.Sigmoid()

    def forward(self, x):
        out = self.avg_pool(x)
        out = self.fc1(out)
        out = self.relu(out)
        out = self.fc2(out)
        weight = self.sigmoid(out)

        return weight


class PSAModule(nn.Module):

    def __init__(self, inplans, planes):
        super(PSAModule, self).__init__()
        conv_kernels = [3, 5, 7, 9]  # 为了获得多尺度特征图, 使用不同大小的卷积核
        stride = 1
        conv_groups = [1, 4, 8, 16]  # 分不同大小的组
        self.conv_1 = conv(inplans, planes // 4, kernel_size=conv_kernels[0], padding=conv_kernels[0] // 2,
                           stride=stride, groups=conv_groups[0])
        self.conv_2 = conv(inplans, planes // 4, kernel_size=conv_kernels[1], padding=conv_kernels[1] // 2,
                           stride=stride, groups=conv_groups[1])
        self.conv_3 = conv(inplans, planes // 4, kernel_size=conv_kernels[2], padding=conv_kernels[2] // 2,
                           stride=stride, groups=conv_groups[2])
        self.conv_4 = conv(inplans, planes // 4, kernel_size=conv_kernels[3], padding=conv_kernels[3] // 2,
                           stride=stride, groups=conv_groups[3])
        self.se = SEWeightModule(planes // 4)
        self.split_channel = planes // 4
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        # 1.利用SPC模块来对通道进行切分，然后针对每个通道特征图上的空间信息进行多尺度特征提取;
        # 通过实现所提出的压缩和压缩（SPC）模块，获得通道方向上的多尺度特征图, x1, x2, x3, x4.
        batch_size = x.shape[0]
        x1 = self.conv_1(x)
        x2 = self.conv_2(x)
        x3 = self.conv_3(x)
        x4 = self.conv_4(x)

        feats = torch.cat((x1, x2, x3, x4), dim=1)  # 0: 按行拼接, 1: 按列拼接
        feats = feats.view(batch_size, 4, self.split_channel, feats.shape[2], feats.shape[3])  # 相当于numpy中resize（）的功能，但是用法可能不太一样。

        # 2. 利用SEWeight模块提取不同尺度特征图的通道注意力，得到每个不同尺度上的通道注意力向量;
        x1_se = self.se(x1)
        x2_se = self.se(x2)
        x3_se = self.se(x3)
        x4_se = self.se(x4)

        x_se = torch.cat((x1_se, x2_se, x3_se, x4_se), dim=1)
        # 3. 利用Softmax对多尺度通道注意力向量进行特征重新标定，得到新的多尺度通道交互之后的注意力权重。
        attention_vectors = x_se.view(batch_size, 4, self.split_channel, 1, 1)
        attention_vectors = self.softmax(attention_vectors)
        # 4. 对重新校准的权重和相应的特征图按元素进行点乘操作，输出得到一个多尺度特征信息注意力加权之后的特征图。
        # 该特征图多尺度信息表示能力更丰富。
        feats_weight = feats * attention_vectors
        for i in range(4):
            x_se_weight_fp = feats_weight[:, i, :, :]
            if i == 0:
                out = x_se_weight_fp
            else:
                out = torch.cat((x_se_weight_fp, out), 1)

        return out

if __name__ == '__main__':
    a = PSAModule(256, 256)
    a.forward(torch.randn(2,3,320,320))
