import torch
import numpy as np
import matplotlib.pyplot as plt

# x_data = torch.tensor([[1.0], [2.0], [3.0], [4.0]])
# y_data = torch.tensor([[6.0], [9.0], [12.0], [15.0]])

k_true = 2  # 待预测值
b_true = 1  # 待预测值
# x_data = np.arange(0, 11)
# 注意, 数据范围越大, 如果出现 nan 则将学习率调小.
x_data = torch.arange(0, 21, dtype=torch.float).view(-1, 1)  # 必须要tensor 类型, 因为 linear() 的 input 必须要tensor
y_true = k_true * x_data + b_true
y_data = y_true


loss_list = []
epochs = 400


# 创建模型
class LinearModel(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.linear = torch.nn.Linear(1, 1)

    def forward(self, x):
        y_pred = self.linear(x)
        return y_pred


# 损失函数和优化器
model = LinearModel()
print(model.state_dict())  # 查看网络参数
criterion = torch.nn.MSELoss(reduction='mean')
optimizer = torch.optim.SGD(model.parameters(), lr=0.001)
# 模型迭代，200 次
for epoch in range(epochs):
    y_pre = model(x_data)
    loss = criterion(y_pre, y_data)
    loss_list.append(loss.item())
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()
    print(model.state_dict())  # 查看网络参数

epoch_list = np.arange(epochs)
print(model.linear.weight.item(), model.linear.bias.item())  # 输出权值和偏差

test_set = torch.tensor([[8.0]])  # 模型预测
print(model(test_set).item())

# 训练过程 loss 变化过程
plt.plot(epoch_list, loss_list, label='loss', color='blue')
plt.show()
