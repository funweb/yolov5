import os
import shutil


bash_dir = r"F:\datasets\trim500_train_test\train"
target_dir = r"F:\datasets\trim500_train_test\llLY"

name_list = os.listdir(bash_dir)

for name in name_list:
    if name.endswith(".txt"):
        with open(os.path.join(bash_dir, name), 'r') as fr:
            lines = fr.readlines()
            for line in lines:
                label = line.split()[0]
                if label == "8":
                    n = name.split(".txt")[0]
                    shutil.copy(os.path.join(bash_dir, n+".txt"), os.path.join(target_dir, n+".txt"))
                    shutil.copy(os.path.join(bash_dir, n + ".jpg"), os.path.join(target_dir, n + ".jpg"))
                    print(name)
