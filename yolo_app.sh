#! /bin/bash
# yolo_app.sh

#git pull origin master > /dev/null 2>&1

function wait_s {
   for i in $(seq $1 -1 1)
    do
        echo -ne "wait: \e[31m $i \e[0m s\r"
        sleep 1
    done
}


# param: array
# param: index
# param: 前缀
# paran: 后缀
# return: value but not: null
function nullORstring() {
        arr=($1)
        i=$2

        if [ "${arr[${i}]}" == "null" ]; then
            echo " "
        else
            echo -e "${3}${arr[${i}]}${4}"
        fi
}


function concat_array2string {
    index=0
    string="[ "
    for i in $1; do
        if [ $index -eq $2 ]; then
            string="   ${string}\e[31m ${index}:${i} \e[0m | "
        else
            string="   ${string}${index}:${i} | "
        fi
        ((index=index+1))
    done
    string="${string:0:0-2} ]"
    echo $string
}


function yolo_app {
    CONF_DIR="configure"

    T=`date +%d%H%M`
    computer_name="$(hostname)_$T"
    echo -e "your_name: ${computer_name}"

    img_array=(64 128 256 320 640 1280)
    img_default_index=4

    epoch_array=(3 300 400 500 1000 2000)
    epoch_default_index=3

    batch_array=(2 4 6 8 12 16 32 64 128)
    batch_default_index=6

    weight_array=("yolov5s" "yolov5m" "yolov5l" "yolov5x" "null")
    weight_default_index=0

    # shellcheck disable=SC2054
    # cfg_array=("yolov5s" "yolov5s_anchor" "yolov5s_PSAModule_14" "yolov5s_PSAModule_18" "yolov5s_PSAModule_19" "yolov5s_PSAModule_22" "yolov5s_PSAModule_23" "yolov5s_PSAModule_23_2", "yolov5s_PSAModule_C3")
    cfg_array=("yolov5s" "yolov5s_anchor" "yolov5s_anchor_PSA_C3" "yolov5m_anchor" "yolov5s_anchor_PSA_C3_backbone" "yolov5s_anchor_PSA_C3_backbone_concat" "yolov5s_anchor_PSA_Conv" "yolov5s_anchor_PSA_Conv_3" "yolov5x")
    cfg_default_index=0

    device_array=(0 1 2 3 "CPU")
    device_defacult_index=0

    hyp_array=("scratch" "scratch_fit" "finetune" "finetune_no_mosaic" "null")
    hyp_default_index=2

    optimizer_array=("--adam" "null")
    optimizer_default_index=1

    noautoanchor_array=("--noautoanchor" "null")
    noautoanchor_default_index=1

    image_weights_array=("--image-weights" "null")
    image_weights_default_index=1


    echo -en "Please enter the img size $(concat_array2string "${img_array[*]}" ${img_default_index}) :\t"; read -t 15 img_index
    echo -en "Please enter the epoch size $(concat_array2string "${epoch_array[*]}" ${epoch_default_index}) :\t"; read -t 15 epoch_index
    echo -en "Please enter the batch size $(concat_array2string "${batch_array[*]}" ${batch_default_index}) :\t"; read -t 15 batch_index
    echo -en "Please enter the weight size $(concat_array2string "${weight_array[*]}" ${weight_default_index}) :\t"; read -t 15 weight_index
    echo -en "Please enter the cfg size $(concat_array2string "${cfg_array[*]}" ${cfg_default_index}) :\t"; read -t 15 cfg_index
    echo -en "Please enter the device size $(concat_array2string "${device_array[*]}" ${device_defacult_index}) :\t"; read -t 15 device_index
    echo -en "Please select the hyp $(concat_array2string "${hyp_array[*]}" ${hyp_default_index}) :\t"; read -t 15 hyp_index

    echo -en "Please select the optimizer $(concat_array2string "${optimizer_array[*]}" ${optimizer_default_index}) :\t"; read -t 15 optimizer_index
    echo -en "Please select the noautoanchor $(concat_array2string "${noautoanchor_array[*]}" ${noautoanchor_default_index}) :\t"; read -t 15 noautoanchor_index
    echo -en "Please select the image_weights $(concat_array2string "${image_weights_array[*]}" ${image_weights_default_index}) :\t"; read -t 15 image_weights_index

    # parser.add_argument('--hyp', type=str, default='data/hyp.scratch.yaml', help='hyperparameters path')
    # parser.add_argument('--noautoanchor', action='store_true', help='disable autoanchor check')  # ²»×Ô¶¯µ÷Õûanchor£¬Ä¬ÈÏFalse
    # parser.add_argument('--adam', action='store_true', help='use torch.optim.Adam() optimizer')  # ÊÇ·ñÊ¹ÓÃadamÓÅ»¯Æ÷
    # parser.add_argument('--image-weights', action='store_true', help='use weighted image selection for training')  # Ê¹ÓÃ¼ÓÈ¨Í¼ÏñÑ¡Ôñ½øÐÐÑµÁ·???
    # nbs=64

    img_default_index=${img_index:-${img_default_index}}
    epoch_default_index=${epoch_index:-${epoch_default_index}}
    batch_default_index=${batch_index:-${batch_default_index}}
    weight_default_index=${weight_index:-${weight_default_index}}
    cfg_default_index=${cfg_index:-${cfg_default_index}}
    device_defacult_index=${device_index:-${device_defacult_index}}
    hyp_default_index=${hyp_index:-${hyp_default_index}}
    optimizer_default_index=${optimizer_index:-${optimizer_default_index}}
    noautoanchor_default_index=${noautoanchor_index:-${noautoanchor_default_index}}
    image_weights_default_index=${image_weights_index:-${image_weights_default_index}}


    param_img=" --img ${img_array[${img_default_index}]}"
    param_epoch=" --epochs ${epoch_array[${epoch_default_index}]}"
    param_batchSize=" --batch-size ${batch_array[${batch_default_index}]}"
    param_data=" --data ${CONF_DIR}/trim500_train_test.yaml"
    param_weight=$(nullORstring "${weight_array[*]}" ${weight_default_index} "/" ".pt")
    param_weight="--weights weights${param_weight}"
    param_device=" --device ${device_array[${device_defacult_index}]}"
    param_cfg=" --cfg ${CONF_DIR}/cfgs/${cfg_array[${cfg_default_index}]}.yaml"
    param_hyp=$(nullORstring "${hyp_array[*]}" ${hyp_default_index} "--hyp ${CONF_DIR}/hyps/hyp." ".yaml")
    param_optimizer=$(nullORstring "${optimizer_array[*]}" ${optimizer_default_index})
    param_noautoanchor=$(nullORstring "${noautoanchor_array[*]}" ${noautoanchor_default_index})
    param_image_weights=$(nullORstring "${image_weights_array[*]}" ${image_weights_default_index})

    param_name="--name ${computer_name}_i${img_default_index}_e${epoch_default_index}_b${batch_default_index}_w${weight_default_index}_c${cfg_default_index}_d${device_defacult_index}_h${hyp_default_index}_o${optimizer_default_index}_n${noautoanchor_default_index}_i${image_weights_default_index}"

    params=(${param_img} \\
        "\n" ${param_epoch} \\
        "\n" ${param_batchSize} \\
        "\n" ${param_data} \\
        "\n" ${param_device} \\
        "\n" ${param_cfg} \\
        "\n" ${param_name} ${param_hyp} ${param_weight} ${param_optimizer} ${param_noautoanchor} ${param_image_weights}
        )
        
    echo -e "train.py \n ${params[*]}"
    
    wait_s 5

    python train.py ${param_img} ${param_epoch} ${param_batchSize} ${param_data} ${param_weight} ${param_device} ${param_cfg} ${param_name} ${param_hyp} ${param_optimizer} ${param_noautoanchor} ${param_image_weights}
}

yolo_app

